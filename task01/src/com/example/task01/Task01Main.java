package com.example.task01;

import java.io.IOException;

public class Task01Main {
  public static void main(String[] args) throws IOException {
    Pair<Integer, String> pair = Pair.of(1, "hello");
    Integer i = pair.getFirst(); // 1
    String s = pair.getSecond(); // "hello"

    System.out.printf("i: %d; s: %s\n", i, s);

    pair.ifPresent((first, second) -> {
      System.out.println(first);
      System.out.println(second);
    });

    Pair<Integer, String> pair2 = Pair.of(1, "hello");
    boolean mustBeTrue = pair.equals(pair2); // true!
    boolean mustAlsoBeTrue = pair.hashCode() == pair2.hashCode(); // true!

    System.out.printf("mustBeTrue: %b; mustAlsoBeTrue: %b\n", mustBeTrue, mustAlsoBeTrue);
  }
}
