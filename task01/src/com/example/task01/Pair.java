package com.example.task01;

import java.util.Objects;
import java.util.function.BiConsumer;

public final class Pair<T, U> {
  private T first;
  private U second;

  private Pair(T first, U second) {
    this.first = first;
    this.second = second;
  }

  public static <T, U> Pair<T, U> empty() {
    return new Pair<T, U>(null, null);
  }

  public static <T, U> Pair<T, U> of(T first, U second) {
    return new Pair<T, U>(first, second);
  }

  public T getFirst() {
    return first;
  }

  public U getSecond() {
    return second;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (!(obj instanceof Pair<?, ?>)) {
      return false;
    }

    Pair<?, ?> that = (Pair<?, ?>) obj;

    return Objects.equals(first, that.first) && Objects.equals(second, that.second);
  }

  public int hashCode() {
    return Objects.hash(first, second);
  }

  public boolean ifPresent() {
    return first != null && second != null;
  }

  public void ifPresent(BiConsumer<? super T, ? super U> consumer) {
    if (first != null && second != null) {
      consumer.accept(first, second);
    }
  }
}
