package com.example.task02;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;

public class SavedList<E extends Serializable> extends AbstractList<E> {
  private ArrayList<E> list;
  private final File file;

  public SavedList(File file) {
    this.file = file;
    readFromFile();
  }

  @SuppressWarnings("unchecked")
  private void readFromFile() {
    try (var stream = new ObjectInputStream(new FileInputStream(file))) {
      list = (ArrayList<E>) stream.readObject();
    } catch (Exception e) {
      list = new ArrayList<>();
    }
  }

  private void writeToFile() {
    try (var stream = new ObjectOutputStream(new FileOutputStream(file))) {
      stream.writeObject(list);
    } catch (Exception e) {}
  }

  @Override
  public E get(int index) {
    return list.get(index);
  }

  @Override
  public E set(int index, E element) {
    var elem = list.set(index, element);
    writeToFile();
    return elem;
  }

  @Override
  public int size() {
    return list.size();
  }

  @Override
  public void add(int index, E element) {
    list.add(index, element);
    writeToFile();
  }

  @Override
  public E remove(int index) {
    var elem = list.remove(index);
    writeToFile();
    return elem;
  }
}
