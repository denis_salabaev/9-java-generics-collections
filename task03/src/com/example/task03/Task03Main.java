package com.example.task03;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import lombok.NonNull;

final class WordInfo {
  public HashMap<Character, Integer> encounters;
  public long len;

  public WordInfo(@NonNull String word) {
    encounters = new HashMap<>();
    len = word.length();

    for (int i = 0; i < len; ++i) {
      char c = word.charAt(i);
      encounters.compute(c, (k, v) -> {
        return (v == null) ? 1 : v + 1;
      });
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (!(obj instanceof WordInfo)) {
      return false;
    }

    WordInfo that = (WordInfo) obj;

    return len == that.len && Objects.equals(encounters, that.encounters);
  }
}

final class AnagramGroup {
  public WordInfo info;
  public TreeSet<String> words;

  public AnagramGroup(@NonNull String word) {
    info = new WordInfo(word);
    words = new TreeSet<>(List.of(word));
  }
}

public class Task03Main {
  public static void main(String[] args) throws IOException {
    List<Set<String>> anagrams = findAnagrams(new FileInputStream("task03/resources/singular.txt"),
      Charset.forName("windows-1251"));

    for (Set<String> anagram : anagrams) {
      System.out.println(anagram);
    }
  }

  public static List<Set<String>> findAnagrams(@NonNull String[] words) {
    HashMap<Integer, ArrayList<AnagramGroup>> anagramsBySize = new HashMap<>();

    Stream.of(words)
      .filter((word) -> word.length() >= 3)
      .filter((word) -> word.matches("[а-яА-Я]*"))
      .map((word) -> word.toLowerCase())
      .forEach((word) -> {
        var info = new WordInfo(word);

        anagramsBySize.compute(word.length(), (len, anagramGroups) -> {
          if (anagramGroups == null) {
            return new ArrayList<>(List.of(new AnagramGroup(word)));
          }

          anagramGroups.stream()
            .filter((anagramGroup) -> anagramGroup.info.equals(info))
            .findAny()
            .ifPresentOrElse(
              (anagramGroup) -> anagramGroup.words.add(word),
              () -> anagramGroups.add(new AnagramGroup(word)));

          return anagramGroups;
        });
      });

    List<Set<String>> result = new ArrayList<>();

    for (var sizedGroup : anagramsBySize.values()) {
      for (var anagramGroup : sizedGroup) {
        if (anagramGroup.words.size() >= 2) {
          result.add(anagramGroup.words);
        }
      }
    }

    result.sort(new Comparator<Set<String>>() {
      @Override
      public int compare(Set<String> lhs, Set<String> rhs) {
        return lhs.iterator().next().compareTo(rhs.iterator().next());
      }
    });

    return result;
  }

  public static List<Set<String>> findAnagrams(InputStream inputStream, Charset charset) {
    var scanner = new Scanner(inputStream, charset);
    scanner.useDelimiter("\n");

    var words = scanner.tokens().toArray(String[]::new);

    return findAnagrams(words);
  }
}
